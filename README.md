# Report template in LaTex
Here is my template for my academic reports. I use LateX as text editor and Zotero for the bibliography.

# How to procide
- For the compilation in TexMaker
     - PPdfLaTeX + Bib(la)tex + PdfLaTex (x2) + See PDF
- For the images
     - Use vectorial images as often as possible
- For the bibliography
     - Use Zotero